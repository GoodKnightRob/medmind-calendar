package rpm.medmind.medmindcalendar.web.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rpm.medmind.medmindcalendar.web.model.MedicationDTO;
import rpm.medmind.medmindcalendar.web.services.MedicationService;

import java.util.UUID;

@RequestMapping("/api/v1/medication")
@RestController
public class MedicationController {

    private final MedicationService medicationService;

    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping({"/{medicationId}"})
    public ResponseEntity<MedicationDTO> getMedication(@PathVariable UUID medicationId){
        return new ResponseEntity<>(medicationService.getMedicationById(medicationId), HttpStatus.OK);
    }
}
