package rpm.medmind.medmindcalendar.web.model;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class MedicationDTO {
    private UUID id;
    private String name;
    private String FrequencyToTake;
    private int amountToTake;

}
