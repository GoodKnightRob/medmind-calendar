package rpm.medmind.medmindcalendar.web.services;

import rpm.medmind.medmindcalendar.web.model.MedicationDTO;

import java.util.UUID;

public interface MedicationService {
    MedicationDTO getMedicationById(UUID medicationId);
}
