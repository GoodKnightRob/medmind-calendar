package rpm.medmind.medmindcalendar.web.services;

import org.springframework.stereotype.Service;
import rpm.medmind.medmindcalendar.web.model.MedicationDTO;

import java.util.UUID;
@Service
public class MedicationServiceImpl implements MedicationService {
    @Override
    public MedicationDTO getMedicationById(UUID medicationId) {
        return MedicationDTO.builder().id(UUID.randomUUID())
                .name("Tylenol")
                .FrequencyToTake("Daily")
                .amountToTake(1)
                .build();

    }
}
