package rpm.medmind.medmindcalendar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedmindCalendarApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedmindCalendarApplication.class, args);
	}

}
